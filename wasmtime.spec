# This requires network!

%global wasi_commit   ef8c1a53feb2dfb763d4ea5c7d9e0a0126b45579
%global c_api_commit  c9d31284651b975f05ac27cee0bab1377560b87e
%global crypto_commit 1f3078088bd0de36a0bc96859a1724e7d338a832
%global nn_commit     8adc5b9b3bb8f885d44f55b464718e24af892c94
%global tests_commit  9994915e0cca8b42a16c577e4c85491822367dde

Name:       wasmtime
Version:    0.28.0
Release:    3%{?dist}
Summary:    Standalone JIT-style runtime for WebAssembly, using Cranelift 

License:    ASL 2.0 with exceptions
URL:        https://wasmtime.dev/

Source0:    https://github.com/bytecodealliance/wasmtime/archive/refs/tags/v%{version}.tar.gz
Source1:    https://github.com/WebAssembly/WASI/archive/%{wasi_commit}/WASI-%{wasi_commit}.tar.gz
Source2:    https://github.com/WebAssembly/wasm-c-api/archive/%{c_api_commit}/wasm-c-api-%{c_api_commit}.tar.gz
Source3:    https://github.com/WebAssembly/wasi-crypto/archive/%{crypto_commit}/wasi-crypto-%{crypto_commit}.tar.gz
Source4:    https://github.com/WebAssembly/wasi-nn/archive/%{nn_commit}/wasi-nn-%{nn_commit}.tar.gz
Source5:    https://github.com/WebAssembly/testsuite/archive/%{tests_commit}/testsuite-%{tests_commit}.tar.gz

# Patch crates/c-api/Cargo.toml to update its version and disable headers generation
Patch0:     patch-c-api-cargo.diff

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging
BuildRequires:  help2man

# The version of cargo-c currently packaged is too old
# BuildRequires:  cargo-c

# Required to build cargo-c
BuildRequires:  openssl-devel

%description
Standalone JIT-style runtime for WebAssembly, using Cranelift.

%package c-api
Summary: Wasmtime C API
%description c-api
This package provides the C API for Wasmtime, a JIT-style runtime for WebAssembly,
using Cranelift.

%package c-api-devel
Summary: Development files for Wasmtime C API
Requires: pkg-config
%description c-api-devel
This package contains libraries and header files for
developing applications that use Wasmtime C API.
             
%prep
%autosetup -p1
# Untar submodules
tar zxf %{SOURCE1} --strip-components=1 -C crates/wasi-common/WASI
tar zxf %{SOURCE2} --strip-components=1 -C crates/c-api/wasm-c-api
tar zxf %{SOURCE3} --strip-components=1 -C crates/wasi-crypto/spec
tar zxf %{SOURCE4} --strip-components=1 -C crates/wasi-nn/spec
tar zxf %{SOURCE5} --strip-components=1 -C tests/spec_testsuite

# Configure cargo
mkdir -p .cargo
cat > .cargo/config << EOF
[build]
rustc = "%{__rustc}"
rustdoc = "%{__rustdoc}"
rustflags = %{__global_rustflags_toml}

[install]
root = "%{buildroot}%{_prefix}"

[term]
verbose = true
EOF

# Install cargo-c
cargo install cargo-c

%build
%cargo_build
cargo cbuild --release --manifest-path crates/c-api/Cargo.toml \
    --destdir=%{buildroot} \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir} \
    --pkgconfigdir=%{_libdir}/pkgconfig

%install
%cargo_install
cargo cinstall --release --manifest-path crates/c-api/Cargo.toml \
    --destdir=%{buildroot} \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir} \
    --pkgconfigdir=%{_libdir}/pkgconfig
# Remove static library
rm -v %{buildroot}%{_libdir}/libwasmtime.a
# Remove source
rm -rf %{buildroot}%{cargo_registry}/wasmtime-cli-%{version}
# Man page
install -d %{buildroot}%{_mandir}/man1
help2man --no-info %{buildroot}%{_bindir}/wasmtime > wasmtime.man
install -p -m 644 -D wasmtime.man %{buildroot}%{_mandir}/man1/wasmtime.1

%if %{with check}
%check
%cargo_test
%endif

%files
%doc README.md RELEASES.md CONTRIBUTING.md SECURITY.md
%doc CODE_OF_CONDUCT.md ORG_CODE_OF_CONDUCT.md
%license LICENSE
%{_bindir}/wasmtime
%{_mandir}/man1/wasmtime.1*

%files c-api
%license LICENSE
%{_libdir}/libwasmtime.so.0*

%files c-api-devel
%doc RELEASES.md CONTRIBUTING.md SECURITY.md
%doc CODE_OF_CONDUCT.md ORG_CODE_OF_CONDUCT.md
%{_includedir}/wasmtime/
%{_libdir}/libwasmtime.so
%{_libdir}/pkgconfig/wasmtime.pc

%changelog
* Sat Jul 03 2021 Olivier Lemasle <o.lemasle@gmail.com> - 0.28.0-3
- Fix build

* Sat Jul 03 2021 Olivier Lemasle <o.lemasle@gmail.com> - 0.28.0-2
- Use latest cargo-c (0.9.0)

* Sun Jun 27 2021 Olivier Lemasle <o.lemasle@gmail.com> - 0.28.0-1
- Initial package


